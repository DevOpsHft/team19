# Start with a core build process

What you find in the beginning is the core build, without any (useful) functionality.

What are the parts:

* Defined the structure of development and build.
* Defined the core stages of the build process.
* Filled out the build stage (only) without a visible result.
* Initial content for the web server (but not used).

## Steps

* Copy `index.html` to the `src` directory.
* Copy `.gitlab-ci.yml` the the root directory.

## Result

* Have a look at `CI/CD > Pipelines`
* You should see a running or already finished build job.
* What is the result?